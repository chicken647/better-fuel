# More Armor

**This mod adds Better Fuel for Minecraft version 1.16.5 - 1.18.2**



Check out all my mods:  _https://linktr.ee/chickenmods_


Longer lasting fuel for better efficiency! 


+ Higher Tier Fuel



Better Fuel Currently Added: 

- Stone Tier Fuel

- Iron Tier Fuel

- Gold Tier Fuel

- Diamond Tier Fuel

- Emerald Tier Fuel

- Netherite Tier Fuel

- Cobalt Tier Fuel

- Cobalt Ore

- Nether Cobalt Ore

- Cobalt Scrap 

- Cobalt Ingot


Want me to add a type of fuel that is not currently available? Post a comment letting me know and ill see what I can do! 
